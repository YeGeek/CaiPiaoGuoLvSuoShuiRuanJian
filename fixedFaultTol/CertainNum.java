package fixedFaultTol;

import java.util.ArrayList;
import java.util.Iterator;

//定百位、十位、个位
public class CertainNum {
	
	 //传入集合、输入的定百位的修改、容错标志flag（flag等于1代表容错，等于0代表不容错）
	public  static ArrayList<Integer>  DanNum_bai (ArrayList<Integer> sn,int[] num,int flag)//定百位
	{
	if(flag==1)//判断是否容错
	{	
			
	Iterator <Integer> iter=sn.iterator();
	
	//过滤
	while (iter.hasNext())
	{
		int n=iter.next();
		int bai=n/100;
		
		
		
		for(int i=0;i<num.length;i++){
			if (bai!=num[i]){
				iter.remove();
			}
		}
		/*if(bai!=num){
			iter.remove();
		}*/
	}
	}
	return sn;
    
  }
	//传入集合、输入的定十位的修改、容错标志flag（flag等于1代表容错，等于0代表不容错）
	public  static ArrayList<Integer>  DanNum_shi (ArrayList<Integer> sn,int[] num,int flag)
	{
		
		if(flag==1)//判断是否容错
		{			
	Iterator <Integer> iter=sn.iterator();
	
	
	while (iter.hasNext())
	{
		int n=iter.next();
		int shi=(n%100)/10;
		for(int i=0;i<num.length;i++){
			if (shi!=num[i]){
				iter.remove();
			}
		}
	}
		}
	return sn;
    
  }
	//传入集合、输入的定个位的修改、容错标志flag（flag等于1代表容错，等于0代表不容错）
	public  static ArrayList<Integer>  DanNum_ge (ArrayList<Integer> sn,int[] num,int flag)
	{
		
		if(flag==1)//判断是否容错
		{					
	Iterator <Integer> iter=sn.iterator();
	
	
	while (iter.hasNext())
	{
		int n=iter.next();
		int ge=(n%100)%10;
		for(int i=0;i<num.length;i++){
			if (ge!=num[i]){
				iter.remove();
			}
		}
	}
		}
	return sn;
    
  }
}
