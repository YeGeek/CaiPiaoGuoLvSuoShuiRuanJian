# 彩票过滤缩水软件
彩票缩水软件 项目人员：魏浩浩，陈伟，叶志鹏，张汉彬。
### 基本功能
- 爬取中国体育彩票排列三排列五每一期开奖号码
- 经过各种过滤筛选出预期开奖号码
- 通过安卓APP给用户使用
  
软件架构
<pre>|--utilReptile包
<pre>    |--reptile类（获取当期开奖号码）
<pre>|--SecFaultTol包
<pre>    |--ResultFault类（获取二次容错返回的集合）
<pre>    |--SelectFaultTol类（二次容错工具类）
<pre>    |--UtilArithmetic类（二次容错工具类）
<pre>|--doubleid_subAndsum包
<pre>    |--DoubleID类（判断双差双和的类）
<pre>    |--DoubleText类（计算双差和双和的方法类）
<pre>|--fixedFaultTol包
<pre>    |-- ZhiAndHe类（判断质数和合数）
<pre>    |--BigAndSmall类（大小类）
<pre>    |-- CertainNum类（定百位十位个位）
<pre>    |--JiAndOu类（奇偶类）
<pre>|--mathod包
<pre>    |-- DanNumGroup类（胆码组类）
<pre>    |--DoubleNum类（二码过滤类）
<pre>    |--KillNum类（杀码类）
<pre>    |--Sum类（和值过滤类）
<pre>|--initnum包
<pre>    |--StartNum类（号码初始化类）