package utilReptile;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class reptile
{

     /**返回当期的中奖号码*/
    public static Map<String,String> doreptile()
    {   
        /**降序*/
        Map map=new TreeMap<String,String>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2.compareTo(o1);
            }
        });
        String html=getHtml();              //调用gethtml方法
        ArrayList list=new ArrayList();     //临时装中奖号码
        ArrayList list1=new ArrayList();    //临时装中奖期数
        Pattern pattern=Pattern.compile("\\d\\s\\d\\s\\d\\s\\d\\s\\d");   //中奖号码的筛选规则
        Matcher matcher=pattern.matcher(html);
        while (matcher.find()){
            list.add(matcher.group());
        }
        Pattern pattern1=Pattern.compile("[1][8]\\d{3}[期]");            //中奖期数的筛选规则
        Matcher matcher1=pattern1.matcher(html);
        while (matcher1.find()){
            String [] str=matcher1.group().split("期");
            if(list1.contains(str[0])) continue;                        //如果有重复跳出本次循环，不添加到集合
            else {
                list1.add(str[0]);
            }
        }
        for(int i=0;i<list.size();i++){
            map.put(list1.get(i), list.get(i));
        }
        return map;
    }
    /**
     * 获取HTML的内容返回字符串*/
    public static String getHtml()              //获取html
    {
        // 定义即将访问的链接
        String url = "https://www.8200.cn/kjh/p5/";
        // 定义一个字符串用来存储网页内容
        String result = "";
        // 定义一个缓冲字符输入流
        BufferedReader in = null;
        try
        {
            // 将string转成url对象
            URL realUrl = new URL(url);
            // 初始化一个链接到那个url的连接
            URLConnection connection = realUrl.openConnection();
            // 开始实际的连接
            connection.connect();
            // 初始化 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            // 用来临时存储抓取到的每一行的数据
            String line;
            while ((line = in.readLine()) != null)
            {
                // 遍历抓取到的每一行并将其存储到result里面
                result += line + "\n";
            }
        } catch (Exception e)
        {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
        } // 使用finally来关闭输入流
        finally
        {
            try
            {
                if (in != null)
                {
                    in.close();
                }
            } catch (Exception e2)
            {
                e2.printStackTrace();
            }
        }
        return result;
    }
}



