package SecFaultTol;

import java.util.ArrayList;
import java.util.Iterator;

import static SecFaultTol.SelectFaultTol.selectFaultTol;

public class ResultFault
{

    /**
     * 保留容错后的内容*/
    public static ArrayList<Integer> containFault(ArrayList<Integer> list,int onoff,int first,int sec)
    {
        /**
         * 定义个十百位*/
        int unit=0;
        int decade=0;
        int hundred=0;
        int result=selectFaultTol(onoff,first,sec);//调用Selecter类的selectFaultTol方法，返回一个int数据。

        ArrayList<Integer> resultList=new ArrayList<Integer>();//定义一个list集合，用于存过滤后的数据。

        /**
         * 判断是否包含，如果包含存到resultList集合中*/
        Iterator<Integer> it=list.iterator();
        while (it.hasNext())
        {
            if(it.next()!=0)
            {
                unit = it.next() % 10;
                decade = it.next() / 10 % 10;
                hundred = it.next()/100;
            }
            if(unit==result||decade==result||hundred==result)
            {
                resultList.add(it.next());
            }
        }
        return resultList;//返回resultList集合
    }
    /**
     * 排除容错后的内容*/
    public static ArrayList<Integer> excludFault(ArrayList<Integer> list,int onoff,int first,int sec)
    {
        /**
         * 定义个十百位*/
        int unit=0;
        int decade=0;
        int hundred=0;
        int result=selectFaultTol(onoff,first,sec);//调用Selecter类的selectFaultTol方法，返回一个int数据。

        /**
         * 判断是否包含，如果包含存到resultList集合中*/
        Iterator<Integer> it=list.iterator();
        while (it.hasNext())
        {
            if(it.next()!=0)
            {
                unit = it.next() % 10;
                decade = it.next() / 10 % 10;
                hundred = it.next()/100;
            }
            if(unit==result||decade==result||hundred==result)
            {
                list.remove(list.indexOf(it.next()));
            }
        }
        return list;//返回resultList集合
    }
}
