package SecFaultTol;

import java.util.ArrayList;

import static SecFaultTol.UtilArithmetic.*;

public class SelectFaultTol
{

    /**
     * 选择二次容错条件*/
    public static int selectFaultTol(int onoff,int first,int sec)
    {
        int result=0;
        if(onoff==0)
        {
            result=tenThdSumHund(first,sec);
        }

        if(onoff==1)
        {
            result=tenThdSubHund(first,sec);
        }
        if(onoff==10)
        {
            result=tenThdSumThd(first,sec);
        }
        if(onoff==11)
        {
            result=tenThdSubThd(first,sec);
        }
        if(onoff==20)
        {
            result=thdSumHundred(first,sec);
        }
        if(onoff==21)
        {
            result=thdSubHundred(first,sec);
        }
        return result;
    }








}
