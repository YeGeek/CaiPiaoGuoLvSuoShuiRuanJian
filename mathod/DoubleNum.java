package mathod;
import java.util.*;
//二码过滤
public class DoubleNum {
	private  static int []NUMBER=new int[6];
	//将三位数拆分成六个二位数，存进新数组
	private  static int[] DN(int num){
		int a = num/100;
		int b = ((num%100)/10);
		int c =((num%100)%10);
		NUMBER[0]=a*10+b;
		NUMBER[1]=a*10+c;
		NUMBER[2]=b*10+a;
		NUMBER[3]=b*10+c;
		NUMBER[4]=c*10+a;
		NUMBER[5]=c*10+b;
		return NUMBER;
	}
	public  static ArrayList<Integer>  DoubleNumber (ArrayList<Integer> sn,int num)
	{
		
			
	Iterator <Integer> iter=sn.iterator();
	//过滤符合二码的数值
	while (iter.hasNext())
	{
		int n=iter.next();
		int[] array=DN(n);
		//遍历长度为6的数组，如果其中一项等于用户选定的二码，则删除
		for(int i=0;i<array.length;i++)
		{
			if (array[i]== num){
			iter.remove();
			break;
			}
		}
		}
	return sn;
	}
}
