package doubleid_subAndsum;
import java.util.ArrayList;
import java.util.Iterator;


public class DoubleID {
	
	 //判断双差ID  与上期双差结果相同则过滤
	public  static ArrayList<Integer>  delDoubleIDSub (ArrayList<Integer> sn,int num)//传入集合和上一期的中奖号码
	{
		
	//迭代		
	Iterator <Integer> iter=sn.iterator();
	
	//过滤
	while (iter.hasNext())
	{
		int n=iter.next();
		int Sub= Double.doubleSub(n);//调用计算双差公式
		num= Double.doubleSub(num);//计算上期中奖号码双差值
		if(Sub==num){//判断是否相同
			iter.remove();//相同则过滤
		}
	}
	return sn;
    
  }
	

	 //判断双差ID  与上期双差结果相同则保留
	public  static ArrayList<Integer>  keepDoubleIDSub (ArrayList<Integer> sn,int num)//传入集合和上一期的中奖号码
	{
		
	//迭代		
	Iterator <Integer> iter=sn.iterator();
	
	//过滤
	while (iter.hasNext())
	{
		int n=iter.next();
		int Sub= Double.doubleSub(n);//调用计算双差公式
		num= Double.doubleSub(num);//计算上期中奖号码双差值
		if(Sub!=num){//判断是否相同
			iter.remove();//相同则保留
		}
	}
	return sn;
   
 }
	
	//判断双和ID,与上期双和结果相同则过滤
	public  static ArrayList<Integer>  delDoubleIDSum (ArrayList<Integer> sn,int num)//传入集合和上一期的中奖号码
	{
		
	//迭代		
	Iterator <Integer> iter=sn.iterator();
	
	//过滤
	while (iter.hasNext())
	{
		int n=iter.next();
		int Sum= Double.doubleSum(n);//调用双和公式
		num= Double.doubleSum(num);//计算上期中奖号码双差值
		if(Sum==num){//判断是否相同
			iter.remove();//相同则过滤
		}
	}
	return sn;
    
  }
	
	//判断双和ID，与上期双和结果相同则保留
	public  static ArrayList<Integer>  keepDoubleIDSum (ArrayList<Integer> sn,int num)//传入集合和上一期的中奖号码
	{
		
	//迭代		
	Iterator <Integer> iter=sn.iterator();
	
	//过滤
	while (iter.hasNext())
	{
		int n=iter.next();
		int Sum= Double.doubleSum(n);//调用双和公式
		num= Double.doubleSum(num);//计算上期中奖号码双差值
		if(Sum!=num){//判断是否相同
			iter.remove();//相同则保留
		}
	}
	return sn;
    
  }
}
	